<?php
	global $wp_version;
	require_once 'gravityform.php';
	require_once 'contact-form-7.php';
	require_once 'ninja-form.php';
	require_once 'contact-form-7-db/contact-form-7-db.php';
	require_once 'contact-form-7-db/contact-form-7-db-ajax.php';
	if ( version_compare( $wp_version, '5.5', '<' ) ) {
		require_once ABSPATH . WPINC . '/class-phpmailer.php';
		require_once ABSPATH . WPINC . '/class-smtp.php';
	} else {
		require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
		require_once ABSPATH . WPINC . '/PHPMailer/SMTP.php';
	}

	class lgFormSummary{

		private static $instance = null;
		private static $dateBreakpoing = 10;

		private function __construct(){
			register_activation_hook(constant('LG_FORM_PLUGIN_PATH') . 'plugin.php', array($this, 'plugin_activation'));
			register_deactivation_hook(constant('LG_FORM_PLUGIN_PATH') . 'plugin.php', array($this, 'plugin_deactivation'));
			add_action('lg_form_summary', array($this, 'init'));
			add_action("wpcf7_before_send_mail", array('lgFormSummaryContactForm7', 'contactform7_form_submission'));
		}

		public static function plugin_activation() {
			global $wpdb;

			$charset_collate = $wpdb->get_charset_collate();
			$table_name1 = $wpdb->prefix . "lg_contactform7_entries";

			$table1 = "CREATE TABLE $table_name1 (
			  id int NOT NULL AUTO_INCREMENT,
			  formid int NOT NULL,
			  submissiondate datetime NOT NULL,
			  PRIMARY KEY  (id)
			) $charset_collate;";

			$table_name2 = $wpdb->prefix . "lg_contactform7_entryfields";

			$table2 = "CREATE TABLE $table_name2 (
			  id int NOT NULL AUTO_INCREMENT,
			  entry_id int NOT NULL,
			  entryname varchar(100),
			  entrydata varchar(100),
			  PRIMARY KEY  (id)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $table1 );
			dbDelta( $table2 );

		    if (! wp_next_scheduled ( 'lg_form_summary' )) {
			wp_schedule_event(time(), '3days', 'lg_form_summary');
		    }
		}

		public static function plugin_deactivation(){
			wp_clear_scheduled_hook('lg_form_summary');
		}

		public static function init(){
			if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
				$entries = lgFormSummaryGravity::getEntries(self::$dateBreakpoing);
				self::send($entries, 'Gravity Form');
			}

			if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
				$entries = lgFormSummaryContactForm7::getEntries(self::$dateBreakpoing);
				self::send($entries, 'Contact Form 7');
			}

			if ( is_plugin_active( 'ninja-forms/ninja-forms.php' ) ) {
				$entries = lgFormSummaryNinja::getEntries(self::$dateBreakpoing);
				self::send($entries, 'Ninja Form');
			}
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFormSummary();
		    }

		    return self::$instance;
		}

		public static function send($entries, $form_type){
			$mailer = new PHPMailer( true );
			$mailer->SMTPDebug = 2;
		    $mailer->isSMTP();                                      // Set mailer to use
		    $mailer->Host = 'smtp.gmail.com';  	// Specify main and backup SMTP servers
		    $mailer->SMTPAuth = true;                               // Enable SMTP authentication
		    $mailer->Username = 'lgformsummary@gmail.com';                 // SMTP username
		    $mailer->Password = '6FpNp0q3%J%&';                           // SMTP password
		    $mailer->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		    $mailer->Port = 465;
		    $mailer->setFrom('lgformtest@gmail.com', 'Longevity Graphics');
		    $mailer->addReplyTo('admin@longevitygraphics.com', 'Longevity Graphics');

		    $mailer->Subject = 'Web form submission report.';

		    $to = get_field('form_summary_report_receipent', 'option');

		    if($to){
			    if(strpos($to, ',') === false){
					$mailer->addAddress($to);
				}else{
					$to = explode(',', $to);
				    for($i = 0; $i < sizeof($to); $i++){
				    	$mailer->addAddress($to[$i]);
				    }
				}

				$mailer->addAddress('admin@longevitygraphics.com');

				try {
				    //Content
				    $mailer->isHTML(true);                                  // Set email format to HTML

				    ob_start();?>

					<html>
						<body>

						    <table>
						    	<tr>
						    		<td style="padding: 30px 30px 0 30px;">
						    			<table>
						    				<tr>
							    				<td>
							    					<table>
												    	<tr>
												    		<td>
												    			<h2>Website submission report for past 3 days</h2>
												    			<h4 style="margin: 0;">By Longevity Graphics</h4>
												    			<h4>Form Plugin: <?php echo $form_type; ?> (For Longevity Graphics Reference)</h4>
												    		</td>

												    	</tr>
												    </table>
							    				</td>
							    			</tr>
							    			<tr>
									    		<td>
									    			<hr style="margin: 40px 0;">
									    		</td>
									    	</tr>
						    			</table>
						    		</td>
						    	</tr>
						    	<tr>
						    		<td style="padding: 0 30px;">
						    			<?php if($entries && is_array($entries) && is_array($entries)): ?>
							    			<?php foreach ($entries as $key => $entry): ?>
							    			<table>
							    				<tr>
							    					<td>
							    						<table clas="form-meta" style="padding-bottom: 20px">
										    				<tr>
										    					<td style="padding: 5px 10px">Form Title:</td>
																<td style="padding: 5px 10px"><?php echo $entry->form_title ?></td>
										    				</tr>
										    				<?php if($entry->source_url): ?>
										    				<tr>
										    					<td style="padding: 5px 10px">Source Url:</td>
										    					<td style="padding: 5px 10px"><?php echo $entry->source_url ?></td>
										    				</tr>
										    				<?php endif; ?>
										    				<tr>
										    					<td style="padding: 5px 10px">Submission Date:</td>
										    					<td style="padding: 5px 10px"><?php echo $entry->submission_date ?></td>
										    				</tr>
										    				<tr>
							    								<td style="padding: 5px 10px">Submission Data:</td>
							    							</tr>
										    			</table>
							    					</td>
							    				</tr>
							    				<tr>
							    					<td>
							    						<table class="fields" style="padding: 5px; background-color: #f7f7f7;">
							    							<?php if($entry->fieldEntries && is_array($entry->fieldEntries) && sizeof($entry->fieldEntries) > 0): ?>
								    							<?php foreach ($entry->fieldEntries as $key => $data): ?>
																	<tr>
																		<td style="padding: 10px">
																			<?php echo $data[0]; ?>
																		</td>
																		<td style="padding: 10px">
																			<?php echo $data[1]; ?>
																		</td>
																	</tr>
								    							<?php endforeach; ?>
								    						<?php endif; ?>
							    						</table>
							    					</td>
							    				</tr>
							    				<tr>
							    					<td>
							    						<hr style="margin: 40px 0;">
							    					</td>
							    				</tr>
							    			</table>
							    			<?php endforeach; ?>
						    			<?php endif; ?>
						    		</td>
						    	</tr>
						    	<tr>
						    		<td style="padding: 0 30px;">
						    			<table>
						    				<tr>
						    					<td>
						    						This is a weekly web form leads report from Longevity Graphics<br>
						    						If you have problem receiving leads from the website. Please email <a href="mailto:srdjana@longevitygraphics.com">srdjana@longevitygraphics.com</a> or call us at <a href="tel:+16049428486">(604) 942 8486</a>
						    					</td>
						    				</tr>
						    				<tr>
						    					<td>
						    						<hr style="margin: 40px 0;">
						    					</td>
						    				</tr>
						    			</table>
						    		</td>
						    	</tr>
						    </table>
						</body>
					</html>

				    <?php $mailer->Body = ob_get_clean();

				    //$mailer->AltBody = 'This is the body in plain text for non-HTML mail clients';

				    $send = $mailer->send();
				    echo 'Message has been sent';
				    lg_write_log('Message has been sent');
				} catch (Exception $e) {
					lg_write_log($mailer->ErrorInfo);
				    echo 'Message could not be sent. Mailer Error: ', $mailer->ErrorInfo;
				}
		    }
		}
	}

	lgFormSummary::getInstance();

?>
