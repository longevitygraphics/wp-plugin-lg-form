<?php
  if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }
	class lgFormSummaryContactForm7DB{

		private static $instance = null;

		private function __construct(){
			add_action( 'admin_menu', array($this, 'registerAdminMenu') );
			add_action( 'admin_enqueue_scripts', array($this, 'load_admin_script') );
		}

		public function registerAdminMenu() {
		    add_submenu_page(
			    'wpcf7',
			    __('Contact Form 7 DB'), 
			    __('Contact Form 7 DB'), 
			    'edit_posts', 
			    'contact-form7-db', 
			    array($this, 'render_template')
		    );
		}

		function load_admin_script($hook) {
	        if($hook != 'contact_page_contact-form7-db') {
	                return;
	        }
	        wp_enqueue_style( 'lg_contact_form_7_db_style', plugins_url('/admin-style.css', __FILE__), array(), filemtime(plugin_dir_path( __FILE__ ) . "/admin-style.css") );
	        wp_enqueue_script( 'lg_contact_form_7_db_script', plugins_url('/admin-script.js', __FILE__), array('jquery'), filemtime(plugin_dir_path( __FILE__ ) . "/admin-script.js"));
		}


		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFormSummaryContactForm7DB();
		    }
		 
		    return self::$instance;
		}

		public function render_template(){
			$forms = WPCF7_ContactForm::find();
			ob_start();
			?>
				<div id="contact-form-7-db">
					<div class="contact-form-db-filter">
						<select id="contact-form-db-form-select">
							<?php foreach ($forms as $key => $form):
								$form_id = $form->id();
								$form_title = $form->title();
							?>
								<option value="<?php echo $form_id; ?>"><?php echo $form_title; ?></option>
							<?php endforeach; ?>
						</select>
						<select id="contact-form-db-time-select">
							<option value="7">7 days</option>
							<option value="14">14 days</option>
							<option value="30">30 days</option>
							<option value="60">60 days</option>
							<option value="all">All</option>
						</select>
						<a id="get-records">Search</a>
					</div>
					<div class="main">
						<div class="main-content">
							
						</div>
					</div>
				</div>
			<?php
			echo ob_get_clean();
		}
	}

	lgFormSummaryContactForm7DB::getInstance();

?>