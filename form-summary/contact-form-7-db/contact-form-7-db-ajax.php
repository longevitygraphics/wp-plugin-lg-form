<?php
  if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }
	class LGContactForm7DBAjax{

		private static $instance = null;

		private function __construct(){
			add_action( 'wp_ajax_lg_form_contact_form_7_db_get_forms', array( $this, 'get_forms' ) );
		}

		function get_forms(){
			global $wpdb;

			$all_entries = array();
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));
			$entries = $this->getSubmissionEntriesByIdAndDate($data[0], $data[1]);

			if($entries && is_array($entries)){
				foreach ($entries as $key => $entry) {
					$fields = $this->getSubmissionFormFieldsById($entry->id);
					array_push($all_entries, array($entry, $fields));
				}
			}
			echo json_encode($all_entries);
			wp_die();
		}

		private function getSubmissionEntriesByIdAndDate($formId, $date){
			global $wpdb;
			$table = $wpdb->prefix . 'lg_contactform7_entries';

			if($date == 'all'){
				$entry = $wpdb->get_results( "SELECT * FROM $table WHERE formid=$formId" );
			}else{
				$entry = $wpdb->get_results( "SELECT * FROM $table WHERE formid=$formId AND date(submissiondate) > CURDATE() - INTERVAL $date DAY" );
			}

			return $entry;
		}

		private function getSubmissionFormFieldsById($entryId){
			global $wpdb;
			$table = $wpdb->prefix . 'lg_contactform7_entryfields';
			$fields = $wpdb->get_results( "SELECT * FROM $table WHERE entry_id=$entryId" );

			return $fields;
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new LGContactForm7DBAjax();
		    }
		 
		    return self::$instance;
		}

	}

	LGContactForm7DBAjax::getInstance();

?>