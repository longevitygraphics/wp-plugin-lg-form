// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){

    	$('#get-records').on('click', function(){
	     	$form_selected = $('#contact-form-db-form-select').val();
	    	$date_selected = $('#contact-form-db-time-select').val();
			var data = {
				'action': 'lg_form_contact_form_7_db_get_forms',
				'data': JSON.stringify([$form_selected, $date_selected])
			};

			jQuery.post(ajaxurl, data, function(response) {
				data = JSON.parse(response);
				$('.main-content').children().remove();

				data.map(function($item){
					var html = "";
					var field_values = $item[1];
					html += "<table class='single-entry'>";
					html += "<tr><td>Submission Date</td><td>" + $item[0].submissiondate + "</td></tr>";

					for(var i = 0; i < field_values.length; i++){
						html += "<tr><td>" + field_values[i].entryname + "</td><td>" + field_values[i].entrydata + "</td></tr>"
					}
					html += "</table>";
					$('.main-content').append(html);
				});
			});   		
    	});

    });

}(jQuery));