<?php

	class lgFormEntry{

		public $form_type;
		public $form_title;
		public $source_url;
		public $submission_date;
		public $fieldEntries;

		function __construct($form_type, $form_title, $source_url, $submission_date, $fieldEntries)
		{
			$this->form_type = $form_type;
			$this->form_title = $form_title;
			$this->source_url = $source_url;
			$this->submission_date = $submission_date;
			$this->fieldEntries = $fieldEntries;
		}

	}

?>