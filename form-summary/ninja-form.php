<?php

	require_once "model.php";

	class lgFormSummaryNinja{

		private static $instance = null;

		private function __construct(){

		}

		public static function getEntries($date){
			$forms = Ninja_Forms()->form()->get_forms();
			$all_entries = array();
			$time_breakpoint = time() - ( 24 * 60 * 60 * $date );

	        foreach ($forms as $key => $form) {
	        	$sub = Ninja_Forms()->form( $form->get_id() )->get_subs();
	        	$fields = Ninja_Forms()->form( $form->get_id() )->get_fields();
	        	$field_array = array();

	        	foreach ($fields as $key => $field) {
	        		$label = $field->get_settings()['label'];
	        		$key = $field->get_id();
	        		array_push($field_array, array($label, $key));
	        	}

		        foreach( $sub as $sub_model ) {
		        	$form_title = $sub_model->get_form_title();
		            $sub_timestamp = strtotime( $sub_model->get_sub_date( 'Y-m-d') );
		            $sub_date = $sub_model->get_sub_date('m/d/Y h:i:sa');
		            if( $sub_timestamp >= $time_breakpoint ) {
		                $field_entries = array();
		                $index = 1;

		                if($field_array && is_array($field_array)){
		                	foreach ($field_array as $key => $field) {
		                		if($sub_model->get_field_value($field[1])){
		                			array_push($field_entries, array($field[0], $sub_model->get_field_value($field[1])));
		                		}
		                	}
		                }

		                $entry = new lgFormEntry('ninja-form', $form_title, '', $sub_date, $field_entries);
		                array_push($all_entries, $entry);
		            }
		        }
	        }

	        $all_entries = self::sortEntriesByTime($all_entries);

	        return $all_entries;
		}

		public static function sortEntriesByTime($entries){
			usort($entries, function($a, $b){
				$ad = $a->submission_date;
				$bd = $b->submission_date;

				if ($ad == $bd) {
				    return 0;
				}

				return $ad > $bd ? -1 : 1;			
			});

			return $entries;
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFormSummaryNinja();
		    }
		 
		    return self::$instance;
		}
	}

?>