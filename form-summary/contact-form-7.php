<?php

	require_once "model.php";

	class lgFormSummaryContactForm7{

		private static $instance = null;

		private function __construct(){

		}

		public static function getEntries($date){
			$forms = WPCF7_ContactForm::find();
			$all_entries = array();

			foreach ($forms as $key => $form) {
				$form_id = $form->id();
				$form_title = $form->title();

				$entries = self::getSubmissionFormById($form_id, $date);
				if($entries && is_array($entries)){
					foreach ($entries as $keyEntry => $entry) {
						$fields = self::getSubmissionFormFieldsById($entry->id);
						$sub_date = $entry->submissiondate;
						$field_entries = array();

						if($fields && is_array($fields)){
							foreach ($fields as $keyField => $field) {
								array_push($field_entries, array($field->entryname, $field->entrydata));
							}
						}

						$entry = new lgFormEntry('contact-form-7', $form_title, '', $sub_date, $field_entries);
						array_push($all_entries, $entry);
					}
				}
			}
			$all_entries = self::sortEntriesByTime($all_entries);

			return $all_entries;
		}

		public static function getSubmissionFormById($formId, $date){
			global $wpdb;
			$table = $wpdb->prefix . 'lg_contactform7_entries';
			$entry = $wpdb->get_results( "SELECT * FROM $table WHERE formid=$formId AND date(submissiondate) > CURDATE() - INTERVAL $date DAY " );

			return $entry;
		}

		public static function getSubmissionFormFieldsById($entryId){
			global $wpdb;
			$table = $wpdb->prefix . 'lg_contactform7_entryfields';
			$fields = $wpdb->get_results( "SELECT * FROM $table WHERE entry_id=$entryId" );

			return $fields;
		}

		public static function contactform7_form_submission($cf7){
			global $wpdb;

		    $wpcf = WPCF7_ContactForm::get_current();
			$submission = WPCF7_Submission::get_instance();
			$form = $submission->get_contact_form();

			$request_url = '';
			$submissiondate = date( 'Y-m-d H:i:s' );
			$form_id = $form->id();
		   	$posted_data = $submission->get_posted_data();

   			$wpdb->insert( 
			    $wpdb->prefix . 'lg_contactform7_entries', 
			    array( 
			        'formid'     => $form_id,
			        'submissiondate'    => $submissiondate,
			    )
			);
			$entry_id = $wpdb->insert_id;

		   	foreach ($posted_data as $key => $value) {
		   		if($key != '_wpcf7' && $key != '_wpcf7_version' && $key != '_wpcf7_locale' && $key != '_wpcf7_unit_tag' && $key != '_wpcf7_container_post' && $key != 'g-recaptcha-response'){
		   			$wpdb->insert( 
					    $wpdb->prefix . 'lg_contactform7_entryfields', 
					    array( 
					        'entry_id'     => $entry_id,
					        'entryname'    => $key,
					        'entrydata'	   => $value
					    )
					);
					$entry_field_id = $wpdb->insert_id;
		   		}
		   	}
		}

		public static function sortEntriesByTime($entries){
			usort($entries, function($a, $b){
				$ad = $a->submission_date;
				$bd = $b->submission_date;

				if ($ad == $bd) {
				    return 0;
				}

				return $ad > $bd ? -1 : 1;			
			});

			return $entries;
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFormSummaryContactForm7();
		    }
		 
		    return self::$instance;
		}
	}

?>