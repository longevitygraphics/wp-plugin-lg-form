<?php

	require_once "model.php";

	class lgFormSummaryGravity{

		private static $instance = null;

		private function __construct(){

		}

		public static function getEntries($date){
			$forms = GFAPI::get_forms();
			$all_entries = array();

			$search_criteria = array();
			$start_date = date( 'Y-m-d', strtotime('-'.$date.' days') );
			$end_date = date( 'Y-m-d', time() );
			$search_criteria['start_date'] = $start_date;
			$search_criteria['end_date'] = $end_date;
			
			for($i = 0; $i < sizeof($forms); $i++){
				$form_id = $forms[$i]['id'];
				$form_title = $forms[$i]['title'];
				$form_fields = array();
				$entries = array();
				for($k = 0; $k < sizeof($forms[$i]['fields']); $k++){
					array_push($form_fields, array($forms[$i]['fields'][$k]->label, $forms[$i]['fields'][$k]->id));
				}

				$single_entries = GFAPI::get_entries($form_id, $search_criteria);
				$entries = array_merge($entries, $single_entries);

				for($j = 0; $j < sizeof($entries); $j++){
					$field_entries = array();

					for($m = 0; $m < sizeof($form_fields); $m++){
						$label = $form_fields[$m][0];
						$data = $entries[$j][(string)$form_fields[$m][1]];

						if($data){
							array_push($field_entries, array($label, $data));
						}
					}

					$entry = new lgFormEntry('gravity-form', $form_title, $entries[$j]['source_url'], $entries[$j]['date_created'], $field_entries);

					array_push($all_entries, $entry);
				}
			}

			$all_entries = self::sortEntriesByTime($all_entries);

			return $all_entries;
		}

		public static function sortEntriesByTime($entries){
			usort($entries, function($a, $b){
				$ad = $a->submission_date;
				$bd = $b->submission_date;

				if ($ad == $bd) {
				    return 0;
				}

				return $ad > $bd ? -1 : 1;			
			});

			return $entries;
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFormSummaryGravity();
		    }
		 
		    return self::$instance;
		}
	}

?>