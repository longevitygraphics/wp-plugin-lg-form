<?php

add_action( 'ninja_forms_after_submission', 'lg_ninja_form_tracking' );

function lg_ninja_form_tracking( $form_data ){

	$form_name = $form_data['settings']['title'];

	lg_form_tracking_send_event($form_name, 'unavailable_ninja_form');

}

?>