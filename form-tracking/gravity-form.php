<?php

function lg_gravity_form_tracking( $entry, $form ) {
 
    $form_name = $form['title'];
 
 	lg_form_tracking_send_event($form_name, 'Gravity Form');
}

add_action( 'gform_after_submission', 'lg_gravity_form_tracking', 10, 2 );

?>