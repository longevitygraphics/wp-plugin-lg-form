<?php

add_action("wpcf7_before_send_mail", "lg_wpcf7_form_tracking");  

function lg_wpcf7_form_tracking($cf7) {

    $wpcf = WPCF7_ContactForm::get_current();

    $form_name = $cf7->name();

    lg_form_tracking_send_event($form_name, 'Contact Form 7');

    return $wpcf;
}

?>