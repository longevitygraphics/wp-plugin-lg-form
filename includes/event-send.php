<?php

	require_once 'ss-ga.class.php';

	function lg_form_tracking_send_event($form_name, $post_name){
		$ga_id = get_field('lg_form_tracking_google_analytics_tracking_id', 'option');
		$url = get_field('lg_form_tracking_web_url', 'option');
		
		$ssga = new lg_form_tracking_ssga( $ga_id, $url );
		$ssga->set_event( 'LG_Tracking', 'Form_Tracking' , $form_name, $post_name );
		$ssga->send();
	}

?>