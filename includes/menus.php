<?php
function lg_form(){
	if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'LG Form Settings',
			'menu_title'	=> 'LG Form',
			'menu_slug' 	=> 'lg_form',
			'capability'	=> 'edit_posts',
			'position'		=> 3,
			'redirect'		=> false,
		));
	}
}

add_action('init', 'lg_form');
?>