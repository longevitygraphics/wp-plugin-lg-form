<?php

	Class lgFormTestContactForm7{

		var $form_title;
		var $notification_email;
		var $form;
		var $domain;

		function __construct(){
			$this->form_title = get_field('lg_form_testing_form_title', 'option');
			$this->notification_email = get_field('lg_form_testing_notification_email', 'option');
			$this->domain = $_SERVER['SERVER_NAME'];
			$this->form = array(
				'id' => -1,
				'title' => $this->form_title,
				'locale' => null,
				'form' => '[submit "Send"]',
				'mail' => array(
						'active' => true,
						'sender' => 'LG FORM TEST - Contact Form 7 <info@longevitygraphics.com>',
						'subject' => $this->domain,
						'recipient' => $this->notification_email,
						'additional_headers' => '',
						'body' => 'Longevity Graphics Plugin to check if the FORM PLUGIN is working properly. Do not change this form at any circumstances. Please Contact Longevity Graphics if you have any questions'
					),
				'mail_2' => null,
				'messages' => null,
				'additional_settings' => null
			);
		}

		function find_form(){
			$form = wpcf7_get_contact_form_by_title($this->form_title);
			return $form;				
		}

		function create_form(){

			$form =	wpcf7_save_contact_form($this->form);

			return $form;
		}

		function check_domain($form){
			$subject = $form->get_properties()['mail']['subject'];

			if($this->domain != $subject){
				$temp = $form->get_properties();
				$temp['mail']['subject'] = $this->domain;
				$form->set_properties($temp);
				$form->save();
			}

			return $form;
		}

		function start(){
			// Find testing form
			$form = $this->find_form();

			// Create testing form if not found
			if(!$form){
				$form = $this->create_form();
			}else{
				$form = $this->check_domain($form);
			}

			//send email only if notification email is set
			if($this->notification_email){
				$result = $form->submit();
			}
			

			var_dump($result);
		}

	}

?>