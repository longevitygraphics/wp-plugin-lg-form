<?php
require_once "gravity-form.php";
require_once "contact-form-7.php";

add_action( 'rest_api_init', function () {
	register_rest_route( 'lg_form_testing/v1', '/run', array(
		'methods' => 'GET',
		'callback' => 'lg_form_testing',
	) );
} );

function lg_form_testing(){
	if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
	  	$lg_form_test_gform = new lgFormTestGForm();
		$lg_form_test_gform->start();
	} 

	if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
		$lg_form_test_contact_form_7 = new lgFormTestContactForm7();
		$lg_form_test_contact_form_7->start();
	}
}

?>