<?php

	Class lgFormTestGForm{

		var $form_title;
		var $notification_email;
		var $default_form;
		var $domain;

		function __construct(){
			$this->form_title = get_field('lg_form_testing_form_title', 'option');
			$this->notification_email = get_field('lg_form_testing_notification_email', 'option');
			$this->domain = $_SERVER['SERVER_NAME'];
			$this->default_form = array(
                'title'          => $this->form_title,
                'description'    => '',
                'labelPlacement' => 'top_label',
                'button'         => array(
                    'type' => 'text'
                ),
                'notifications' => array(
                    array(
                        'id'        => uniqid( '0' ),
                        'name'      => 'LG Notification',
                        'to'        => $this->notification_email,
                        'toType'    => 'email',
                        'event'     => 'form_submission',
                        'subject'   => $this->domain,
                        'message'   => '{all_fields}',
                        'from'      => 'LG FORM TEST - Gravity',
                        'fromName'  => 'LG FORM TEST - Gravity'
                    )
                ),
                'confirmations'  => array(
                    array(
                        'id' => 0,
                        'name' => 'Default Confirmation',
                        'type' => 'message',
                        'message' => 'Longevity Graphics Plugin to check if the FORM PLUGIN is working properly. Do not change this form at any circumstances. Please Contact Longevity Graphics if you have any questions',
                        'isDefault' => true,
                    ),
                ),
                'fields'         => array(
                ),
            );
		}

		function find_form(){
			$forms = GFAPI::get_forms();
			$find = false;

			foreach ($forms as $key => $value) {
				if($value['title'] === $this->form_title){
					$find = true;
				}
			}

			return $find;
		}

		function create_form(){
			$form =	$this->default_form;
			
			return $form = GFAPI::add_form( $form );
		}

		function get_form(){
			$forms = GFAPI::get_forms();

			foreach ($forms as $key => $value) {
				if($value['title'] === $this->form_title){
					return $value;
				}
			}
		}

		function check_domain($form){
			$subject = reset($form['notifications'])['subject'];

			if($this->domain == $subject){
				return $form;
			}else{
				GFAPI::delete_form( $form['id'] );
				$form = $this->create_form();
				return GFAPI::get_form( $form );
			}
		}

		function submit_form( $form_id ){
			$result = GFAPI::submit_form( $form_id, [] );

			return $result;
		}

		function start(){
			// Find testing form
			$find_form = $this->find_form();

			if(!$find_form){
				// Create testing form if not found
				$form = $this->create_form();
				$form = GFAPI::get_form( $form );
			}else{
				// Get testing form
				$form = $this->get_form();

				$form = $this->check_domain($form);
			}
			
			//send email only if notification email is set
			if($this->notification_email){
				$result = $this->submit_form($form['id']);
			}

		}

	}

?>