<?php
/**
* Plugin Name: LG Form
* Plugin URI: https://www.longevitygraphics.com/
* Author: Kelvin Xu,
* Version: 1.2
*/

/**
* Register the main menu page.
*/

define( 'LG_FORM_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

require_once 'form-summary/main.php';
//require_once 'form-reporting/form-reporting.php';

class lgForm{

	function __construct(){
		$this->dependencies_check();
		add_filter( 'cron_schedules', array($this, 'lg_form_cron_schedule') );
		add_filter('acf/settings/load_json', array($this, 'acf_json_load_point'));
	}

	function init(){
		require_once 'main.php';
	}

	function acf_json_load_point( $paths ) {
	    // append path
	    $paths[] = plugin_dir_path( __FILE__ ) . '/acf-json';
	    return $paths;
	}

	function dependencies_check() {
	    if (!function_exists('get_plugins')) {
	        require_once ABSPATH . 'wp-admin/includes/plugin.php';
	    }
	    $plugins = get_plugins();
	    $error_message = '';

	    if (isset($plugins['advanced-custom-fields-pro/acf.php']) || isset($plugins['advanced-custom-fields/acf.php'])) {

			if(!is_plugin_active('advanced-custom-fields-pro/acf.php') && !is_plugin_active('advanced-custom-fields/acf.php')){
				$error_message .= 'LG Form : Both ACF and ACF Pro are not activated.';
			}

	    }else{
	    	$error_message .= 'LG Form : ACF/ACF PRO is not detected';
	    }

		if($error_message == ''){
			add_action( 'plugins_loaded', array($this, 'init') );
		}else{
			add_action( 'admin_notices', function() use ($error_message) {
				$class = 'notice notice-error';
				$this->admin_notice__error($error_message);
			}
		 );
		}
	}

	function admin_notice__error($message) {
		$class = 'notice notice-error';

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	}

	static function lg_form_cron_schedule( $schedules ) {
		// add a 'weekly' schedule to the existing set
		$schedules['weekly'] = array(
			'interval' => 604800,
			'display' => __('Once Weekly')
		);
		$schedules['3days'] = array(
			'interval' => 259200,
			'display' => __('Three days a week')
		);
		return $schedules;
	}


}

	new lgForm();
?>
