<?php
  if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }
	require_once plugin_dir_path( __DIR__ ) . 'form-summary/gravityform.php';
	require_once plugin_dir_path( __DIR__ ) . 'form-summary/contact-form-7.php';
	require_once plugin_dir_path( __DIR__ ) . 'form-summary/ninja-form.php';
	require_once 'form-reporting-ajax.php';

	class lgFormReport{

		private static $instance = null;

		private function __construct(){
			add_action( 'admin_menu', array($this, 'registerAdminMenu') );
			add_action( 'admin_enqueue_scripts', array($this, 'load_admin_script') );
			register_activation_hook(constant('LG_FORM_PLUGIN_PATH') . 'plugin.php', array($this, 'plugin_activation'));
			register_deactivation_hook(constant('LG_FORM_PLUGIN_PATH') . 'plugin.php', array($this, 'plugin_deactivation'));
			add_action('lg_form_report', array($this, 'init'));
		}

		public static function init(){
			LGFormReportAjax::generate_csv(60);
		}

		public static function plugin_activation() {
		    if (! wp_next_scheduled ( 'lg_form_report' )) {
			wp_schedule_event(time(), 'daily', 'lg_form_report');
		    }
		}

		public static function plugin_deactivation(){
			wp_clear_scheduled_hook('lg_form_report');
		}

		public function registerAdminMenu() {
			add_menu_page(
			    __('LG Report'), 
			    __('LG Report'), 
			    'edit_posts', 
			    'lg_form_report', 
			    array($this, 'render_template'),
			    'dashicons-admin-site',
    			4
		    );
		}

		public function render_template(){
			ob_start();
			?>
			<h5>LG Monthly Client Report</h5>
			<p>This feature generates website form lead into csv format to be imported to Longevity Reporting Software.</p>
			<p>The report is generating monthly. To manually generate the report, click below button</p>
			<p>To discard a csv file from a unused form, simply remove the csv file from /wp-upload/lg_form_csv folder. Make sure the form does not exist before remove the csv file.</p>
			<select id="contact-form-report-time-select">
				<option value="1">1 days</option>
				<option value="3">3 days</option>
				<option value="7">7 days</option>
				<option value="14">14 days</option>
				<option value="30">30 days</option>
				<option value="60">60 days</option>
			</select>
			<a id="generate">Generate</a>

			<table id="results"></table>
			<?php
			echo ob_get_clean();
		}

		public function load_admin_script($hook) {
	        if($hook != 'toplevel_page_lg_form_report') {
	                return;
	        }
	        wp_enqueue_style( 'lg_form_report_style', plugins_url('/admin-style.css', __FILE__), array(), filemtime(plugin_dir_path( __FILE__ ) . "/admin-style.css") );
	        wp_enqueue_script( 'lg_form_report_script', plugins_url('/admin-script.js', __FILE__), array('jquery'), filemtime(plugin_dir_path( __FILE__ ) . "/admin-script.js"));
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFormReport();
		    }
		 
		    return self::$instance;
		}
	}

	lgFormReport::getInstance();

?>