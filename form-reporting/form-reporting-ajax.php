<?php
  if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }

  	require_once plugin_dir_path( __DIR__ ) . 'form-summary/gravityform.php';
	require_once plugin_dir_path( __DIR__ ) . 'form-summary/contact-form-7.php';
	require_once plugin_dir_path( __DIR__ ) . 'form-summary/ninja-form.php';

	class LGFormReportAjax{

		private static $instance = null;
		private static $dateBreakpoing;

		private function __construct(){
			add_action( 'wp_ajax_lg_form_report_generate_csv', array( $this, 'generate_csv' ) );
			add_action( 'wp_ajax_lg_form_report_get_csv', array( $this, 'getCsvList' ) );
		}

		static function generate_csv($date = null){
			$all_entries = array();
			$all_fields = array();

			if(array_key_exists('data', $_REQUEST)){
				$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));
				self::$dateBreakpoing = $data[0];
			}else{
				self::$dateBreakpoing = $date;
			}

			if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
				$entries = lgFormSummaryGravity::getEntries(self::$dateBreakpoing);
				$all_entries = array_merge($all_entries, $entries);
			} 

			if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
				$entries = lgFormSummaryContactForm7::getEntries(self::$dateBreakpoing);
				$all_entries = array_merge($all_entries, $entries);
			}

			if ( is_plugin_active( 'ninja-forms/ninja-forms.php' ) ) {
				$entries = lgFormSummaryNinja::getEntries(self::$dateBreakpoing);
				$all_entries = array_merge($all_entries, $entries);
			}

			foreach ($all_entries as $key => $entry) {
				foreach ($entry->fieldEntries as $key => $fieldEntry) {
					if(array_search($fieldEntry[0], $all_fields) == FALSE){
						array_push($all_fields, $fieldEntry[0]);
					}
				}
			}

			$all_entries = self::restructureEntries($all_entries);
			self::writeCsv($all_entries);
			echo json_encode($all_entries);
			wp_die();
		}

		static function restructureEntries($entries){
			$formatted_entries = array();
			if($entries && is_array($entries)){
				foreach ($entries as $key => $entry) {

					if(!array_key_exists($entry->form_type.'_'.$entry->form_title, $formatted_entries)){
						$formatted_entries[$entry->form_type.'_'.$entry->form_title] = array();
					}
					array_push($formatted_entries[$entry->form_type.'_'.$entry->form_title], $entry);
				}
			}

			return $formatted_entries;
		}

		static function writeCsv($entries){
			$upload_dir   = wp_upload_dir();
			$dirname = $upload_dir['basedir'].'/lg_form_csv/';
			 
			if ( !file_exists( $dirname ) ) {
		    	wp_mkdir_p( $dirname );
		    }
			
			if($entries && is_array($entries)){
				foreach ($entries as $key => $entry) {
					$fields = array_map(function($field) { return $field[0]; }, $entry[0]->fieldEntries);
					$filename = $key.'.csv';
					
					$fp = fopen($dirname . $filename,"w");
					fputcsv($fp, $fields);
					foreach ($entry as $key => $single_entry) {
						$field_values = array();
						foreach ($single_entry->fieldEntries as $key => $value) {
							array_push($field_values, $value[1]);
						}
						fputcsv($fp, $field_values);
					}
				}
			}
		}

		function getCsvList(){
			$upload_dir   = wp_upload_dir();
			$dirname = $upload_dir['basedir'].'/lg_form_csv/';
			$url_base = content_url( '/uploads/lg_form_csv/' );

			$files = array_diff(scandir($dirname), array('..', '.'));
			$files = array_map(function($field) use ($url_base){
				return $url_base . $field;
			}, $files);
			$files = array_values($files);
			
			echo json_encode($files);
			wp_die();
		}

		function removeDir($dir){
			$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it,
			             RecursiveIteratorIterator::CHILD_FIRST);
			foreach($files as $file) {
			    if ($file->isDir()){
			        rmdir($file->getRealPath());
			    } else {
			        unlink($file->getRealPath());
			    }
			}
			rmdir($dir);
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new LGFormReportAjax();
		    }
		 
		    return self::$instance;
		}

	}

	LGFormReportAjax::getInstance();

?>