// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){

    	$('#generate').on('click', function(){
    		var data = {
				'action': 'lg_form_report_generate_csv',
				'data': JSON.stringify([$('#contact-form-report-time-select').val()])
			};

			jQuery.post(ajaxurl, data, function(response) {
				data = JSON.parse(response);
				
				var data = {
					'action': 'lg_form_report_get_csv'
				};

				jQuery.post(ajaxurl, data, function(response) {
					data = JSON.parse(response);
					element = $('#results');
					element.children().remove();
					element.append('<tr><td>Csv File Url</td></tr>');
					data.map(function($item){
						$('#results').append('<tr><td><a href="' + $item + '">' + $item + '</a></td></tr>');
					});
				});  
			});  
    	});

    });

}(jQuery));